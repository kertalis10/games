﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainSceneScript : MonoBehaviour {  
    public static ObjectPoolScript.ObjectPool astObjectPool;
    public static ObjectPoolScript.ObjectPool shotObjectPool;
    public static ObjectPoolScript.ObjectPool enemyShotObjectPool;
    public static ObjectPoolScript.ObjectPool oreObjectPool;
    public static ObjectPoolScript.ObjectPool ammoBoxObjectPool;
    public static ObjectPoolScript.ObjectPool lifeBoxObjectPool;
    public static ObjectPoolScript.ObjectPool enemyObjectPool;

    public Text ammoText;
    public Text scoreText;
    public Text shieldsText;
    public Text healthText;
    public GameObject canvas;
    public Transform spawnPoint;
    GameObject player;   
    int money;
    float astCount;   
    bool isPaused;
    bool isGameOn;

	// Use this for initialization
	void Start () {
       
        astObjectPool = new ObjectPoolScript.ObjectPool(20, new string[]{"Asteroids Pack/Assets/Prefabs/Asteroid 1",
                                       "Asteroids Pack/Assets/Prefabs/Asteroid 2",
                                       "Asteroids Pack/Assets/Prefabs/Asteroid 3"});
        shotObjectPool = new ObjectPoolScript.ObjectPool(20, new string[] { "Shot/Bolt" });
        enemyShotObjectPool = new ObjectPoolScript.ObjectPool(20, new string[] { "Shot/EnemyBolt" });
        oreObjectPool = new ObjectPoolScript.ObjectPool(20, new string[] { "Toon Crystals pack/Prefabs/GemStone02" });
        ammoBoxObjectPool = new ObjectPoolScript.ObjectPool(20, new string[] { "Simple Box Pack/AmmoBox" });
        lifeBoxObjectPool = new ObjectPoolScript.ObjectPool(20, new string[] { "Simple Box Pack/LifeBox" });
        enemyObjectPool = new ObjectPoolScript.ObjectPool(20, new string[] { "AstraStarshipPackage1/Prefabs/Enemy" });

        player = Instantiate(StartMenuScript.player, spawnPoint.position, Quaternion.identity);
        player.GetComponent<PlayerController>().enabled = true;
        
        money = 0;
        astCount = 0;
        isPaused = false;
        isGameOn = true;        
        Time.timeScale = 1;       

        AddAsteroid();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {           
            Pause();
        }

        astCount += Time.deltaTime;       
        if (astCount >= 1)
        {
            astCount = 0;
            if (3 >= Random.Range(0, 10))
            {
                AddEnemy();
            }
            else
            {
                AddAsteroid();
            }
        }

        ammoText.text = "Ammo: " + PlayerController.ammo.ToString();
        scoreText.text = "Score: " + PlayerController.score.ToString();
        shieldsText.text = "Shields: " + PlayerController.curShields.ToString();
        healthText.text = "Life:" + PlayerController.curHealth.ToString();

        if (PlayerController.curHealth <= 0 && isGameOn)
        {
            Pause();
            isGameOn = false;
        }
	}

    void Pause()
    {
        if (canvas.gameObject.activeInHierarchy == false)
            ShowPauseMenu();
        else
            HidePauseMenu();
    }

    void AddAsteroid()
    {
        astObjectPool.GetObject(new Vector3(Random.Range(-10, 10), 20, -90));
    }
    void AddEnemy()
    {
        enemyObjectPool.GetObject(new Vector3(Random.Range(-10, 10), 20, -90));
    }

    public void QuitToMainMenu()
    {       
        isGameOn = false;
        SceneManager.LoadScene("0");
    }

    public void ShowPauseMenu()
    {
        canvas.gameObject.SetActive(true);
        isPaused = true;
        Time.timeScale = 0;
    }

    public void HidePauseMenu()
    {
        canvas.gameObject.SetActive(false);
        Time.timeScale = 1;     
        isPaused = false;
    }
}
