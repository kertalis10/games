﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour 
{
    public float speed;
    public float aSpeed;
    public int health;
    private int curHealth;
    private float fireRate = 0.75f;
    private float nextFire;

    void Start () 
    {
        curHealth = health;
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * aSpeed;
        GetComponent<Rigidbody>().velocity = transform.forward * speed;

        //velocity почему-то не работает. Пришлось так
        if(this.tag.Contains("Box"))
            GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, speed * 30));

    }

    private void Update()
    {      
        //Отключение объекта за пределами экрана
        if (!(this.transform.position.z < -90 && this.transform.position.z > -110))
        {
            gameObject.SetActive(false);
            curHealth = health;
        }

        //Создание выстрела вражеского корабля
        if (this.tag.Contains("Ship") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            MainSceneScript.enemyShotObjectPool.GetObject(new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1));
        }       
    }

    public void OnTriggerEnter(Collider other)
    {      
        curHealth--;
        ProcessHit(other.tag);
        if (curHealth <= 0)
        {            
            gameObject.SetActive(false);
            curHealth = health;
            CreateInstance(this.tag);
        }
    }

    private void ProcessHit(string s)
    {
        if (s.Contains("Shot"))
        {
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), this.transform.position, this.transform.rotation);
        }
        if (s == "Shot")
        {
            if (this.tag == "Ast1") PlayerController.score += 5;
            else if (this.tag == "Ast2") PlayerController.score += 10;
            else if (this.tag == "Ast3") PlayerController.score += 15;
            else if (this.tag == "EnemyShip") PlayerController.score += 25;
        }
    }

    private void CreateInstance(string s)
    {
        if (s.Contains("Ast"))
        {
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), this.transform.position, this.transform.rotation);
            if (health >= Random.Range(0, 10))
                MainSceneScript.oreObjectPool.GetObject(this.transform.position);
        }

        if (s.Contains("Ship"))
        {
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), this.transform.position, this.transform.rotation);
            if (8 >= Random.Range(0, 10))
            {
                MainSceneScript.ammoBoxObjectPool.GetObject(this.transform.position);
            }
            else
            {
                MainSceneScript.lifeBoxObjectPool.GetObject(this.transform.position);
            }
        }
    }

}
