﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    public int health;
    public int shields;
    public static int curHealth;
    public static int curShields;
    public static int ammo;
    public static int score;
    public Boundary boundary;
    public Transform[] shotSpawn;
    private float fireRate = 0.25f;
    private float nextFire;
    private float shieldRecoveryRate = 5f;
    private bool isShieldRecoveryStarted;
    private Rigidbody rigidbody;
   

    private void Start()
    {
        curHealth = health;
        curShields = shields;
        isShieldRecoveryStarted = false;
        rigidbody = GetComponent<Rigidbody>();
        ammo = health * 100;
    }

    private void Update()
    {
        if (curHealth <= 0)
        {
            Destroy(this.gameObject);
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), this.transform.position, this.transform.rotation);
        }

        if (curShields < shields && !isShieldRecoveryStarted)
        {
            isShieldRecoveryStarted = true;
            StartCoroutine(RecoverShield());
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rigidbody.velocity = movement * speed;

        rigidbody.position = new Vector3
        (
            Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
            20.0f,
            Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
        );

        rigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidbody.velocity.x * -tilt);

        if (Input.GetKey("space") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            foreach (var v in shotSpawn)
            {
                if (ammo > 0)
                {
                    MainSceneScript.shotObjectPool.GetObject(v.position);
                    ammo--;
                }
            }
        }
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ast1" || other.tag == "EnemyShip" || other.tag == "EnemyShot")
        {
            Hit(1);
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), other.transform.position, this.transform.rotation);  
        }
        if (other.tag == "Ast2")
        {
            Hit(2);
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), other.transform.position, this.transform.rotation);          
        }
        if (other.tag == "Ast3")
        {
            Hit(3);
            Instantiate(Resources.Load<GameObject>("Simple Explosion/Prefabs/Explosion1"), other.transform.position, this.transform.rotation);          
        }
        if (other.tag == "Ore")
        {
            score += 100;
        }
        if (other.tag == "AmmoBox")
        {
            ammo += 10;
        }
        if (other.tag == "LifeBox" && curHealth < health)
        {
            curHealth++;
        }
        other.gameObject.SetActive(false);    
    }

    void Hit(int hit)
    {
        for (int i = 0; i < hit; i++)
        {
            if (curShields > 0)
                curShields--;
            else if (curHealth > 0)
                curHealth--;
        }        
    }

    private IEnumerator RecoverShield()
    {
        yield return new WaitForSeconds(shieldRecoveryRate);
        curShields++;
        isShieldRecoveryStarted = false;
    }

}
