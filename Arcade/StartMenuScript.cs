﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenuScript : MonoBehaviour {

    // Use this for initialization
    public static GameObject player;
    public Text nameOfObject;
    public Canvas menuCanvas;
    public Canvas resultCanvas;
    public GameObject spawnPoint;
    public GameObject[] playerObjectsPrefabs;

    private GameObject[] playerObjects;

    private int countOfObjects;
    private int currentObjectnumber;

    void Start()
    {
        countOfObjects = playerObjectsPrefabs.Length;
        currentObjectnumber = 0;

        playerObjects = new GameObject[countOfObjects];

        for (int i = 0; i < countOfObjects; i++)
        {
            playerObjects[i] = Instantiate(playerObjectsPrefabs[i], spawnPoint.transform, false);           
            if (i == currentObjectnumber)
                playerObjects[i].SetActive(true);
            else
                playerObjects[i].SetActive(false);
        }

        nameOfObject.text = playerObjects[currentObjectnumber].tag;
    }

    // Update is called once per frame
    public void PrevObject()
    {
        ChangeObject('-');
    }

    public void NextObject()
    {
        ChangeObject('+');
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void LoadLevel()
    {
        player = playerObjectsPrefabs[currentObjectnumber];
        SceneManager.LoadScene("1");
    }

    public void ShowResults()
    {
        spawnPoint.SetActive(false);
        menuCanvas.gameObject.SetActive(false);
        resultCanvas.gameObject.SetActive(true);
    }

    public void ShowMenu()
    {
        spawnPoint.SetActive(true);
        menuCanvas.gameObject.SetActive(true);
        resultCanvas.gameObject.SetActive(false);
    }

    private void ChangeObject(char c)
    {
        playerObjects[currentObjectnumber].SetActive(false);
        if (c == '-')
        {            
            if (currentObjectnumber == 0)
                currentObjectnumber = countOfObjects - 1;
            else
                currentObjectnumber--;           
        }
        else
        {
            if(currentObjectnumber == countOfObjects - 1)
                currentObjectnumber = 0;
            else
                currentObjectnumber++;
        }
        playerObjects[currentObjectnumber].SetActive(true);
        nameOfObject.text = playerObjects[currentObjectnumber].tag;
    }
}
