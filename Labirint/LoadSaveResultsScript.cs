﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class LoadSaveResultsScript : MonoBehaviour {

    [System.Serializable]
    public struct PlayerInfo
    {
        public string name;
        public int score;
        public string timeInLabirint;
        public string gameStartDate;
        public string reasonOfEnding;

        public PlayerInfo(string _name, int _score, string _timeInLabirint, string _gameStartDate, string _reasonOfEnding)
        {
            name = _name;
            score = _score;
            timeInLabirint = _timeInLabirint;
            gameStartDate = _gameStartDate;
            reasonOfEnding = _reasonOfEnding;
        }

        public override string ToString()
        {
            return string.Format("{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}", name, score, timeInLabirint, gameStartDate, reasonOfEnding);
        }
    }

    public List<PlayerInfo> listOfGames;

    private void OnApplicationQuit()
    {
        if (MainScript.timeOfGame > 0)
        {
            if (File.Exists("gameinfo.xml")) listOfGames = LoadResults<PlayerInfo>("gameinfo.xml"); else listOfGames = new List<PlayerInfo>();

            string reasonOfEnding;

            if (PlayerController.deadFromZombie) reasonOfEnding = "Зомби"; else reasonOfEnding = "Выход";

            PlayerInfo pl = new PlayerInfo(StartMenu.nameOfPlayer, ScoreScipt.scoreValue, System.Math.Round(MainScript.timeOfGame, 1).ToString(), System.DateTime.Now.ToString(), reasonOfEnding);

            listOfGames.Add(pl);            

            SaveResults(listOfGames.OrderByDescending(x => x.gameStartDate).ToList(), "gameinfo.xml");
        }
    }

    //Загрузка результатов из файла
    public List<T> LoadResults<T>(string path)
    {
        List<T> list;

        System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

        using (FileStream fs = new FileStream(path, FileMode.Open))
            list = (List<T>)formatter.Deserialize(fs);                

        return list;
    }

    //Сохранение результатов в файл
    public void SaveResults<T>(List<T> list, string path)
    {   
        System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));   

        using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            formatter.Serialize(fs, list);
    }
}
