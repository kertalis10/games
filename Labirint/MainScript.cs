﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

class MainScript : MonoBehaviour {

    //Класс отвечающий за генерацию лабиринта
    public sealed class Labirint
    {
        //Вспомогательная структура, в которой хранятся координаты комнаты в лабиринте
        private struct Room
        {
            public int height; 
            public int width;


            public Room(int _height, int _width)
            {
                height = _height;
                width = _width;
            }
        }

        //Рамеры лабиринта
        private int height; 
        private int width;

        //Реальные размеры лабиринта
        private int hidenWidth;
        private int hidenHeight;

        //Двумерный массив в котором хранятся символьный тип части лабиринта(стена, комната и т.д)
        private char[,] labirint;

        //Количество ячеек в лабиринте
        private int count;

        //Конструктор
        public Labirint(int _height, int _width)
        {
            width = _width;
            height = _height;
            count = _height * _width;
            hidenWidth = width * 2 + 1;
            hidenHeight = height * 2 + 1;
            labirint = new char[hidenHeight, hidenWidth];
            GenBlanc();
            Generate();
        }

        //Создание болванки под лабиринт. На выходе получается решетка, где ячейки с четными координатами это стены(С), с нечетными комнаты(К)
        private void GenBlanc()
        {
            for (int i = 0; i < hidenHeight; i++)
                for (int j = 0; j < hidenWidth; j++)
                    if (i % 2 == 0 || j % 2 == 0)
                        labirint[i, j] = 'C';
                    else
                        labirint[i, j] = 'K';
        }

        //Генерация лабиринта
        private void Generate()
        {
            System.Random rand = new System.Random();
            Stack<Room> stack = new Stack<Room>();
            Dictionary<char, Room> dict;
            List<char> list;
            Room curRoom = new Room((rand.Next(this.height) + 1) * 2 - 1, 1); //Стартовая позиция
            Room nextRoom;
            char sideOfNextRoom;
            labirint[curRoom.height, curRoom.width] = 'T';            

            labirint[curRoom.height, curRoom.width - 1] = 'S'; //Точка входа в лабиринт 
            labirint[rand.Next(this.height) * 2 + 1, hidenWidth - 1] = 'F'; //Точка выхода из лабиринта 
            stack.Push(curRoom);

            while (count > 1)                //если есть непосещенные комнаты в лабиринте
            {
                dict = GetNeibours(curRoom); // сканируем соседние комнаты относительно нынешней позиции
                if (dict.Count != 0)         //если есть непосещенные продолжается путь
                {
                    list = dict.Keys.ToList();
                    sideOfNextRoom = list[rand.Next(list.Count)];   //случайный выбор следующей комнаты
                    nextRoom = dict[sideOfNextRoom];
                    labirint[curRoom.height, curRoom.width] = '*';
                    labirint[nextRoom.height, nextRoom.width] = 'T';
                    switch (sideOfNextRoom)
                    {
                        case 'L':
                            labirint[curRoom.height, curRoom.width - 1] = '*';
                            break;
                        case 'U':
                            labirint[curRoom.height - 1, curRoom.width] = '*';
                            break; ;
                        case 'R':
                            labirint[curRoom.height, curRoom.width + 1] = '*';
                            break; ;
                        case 'D':
                            labirint[curRoom.height + 1, curRoom.width] = '*';
                            break; ;
                        default: break;
                    }
                    curRoom = nextRoom;
                    stack.Push(curRoom);                        
                    dict = GetNeibours(curRoom);
                    count--;
                }
                else                    //если непосещенных комнат нет совершается шаг назад
                    curRoom = stack.Pop();
            }

            AddAdditionalPassage();

        }

        //Добавление дополнительных проходов в сгенерированный лабиринт
        private void AddAdditionalPassage()
        {
            int countOfPassage = (width * height) / 3; //колличество проходов в зависимости от размера лабиринта
            int heightRandom;
            int widthRandom;

            while(countOfPassage > 0)
            {
                //генерируем координаты будущего прохода
                heightRandom = Random.Range(1, hidenHeight - 1);
                widthRandom = Random.Range(1, hidenWidth - 1);
                //если по координатам находится стена
                if (labirint[heightRandom, widthRandom] == 'C')
                {
                    //если в соседних ячейках, либо по горизонтали, либо по вертикали, также присутствуют стены, добавляем проход  
                    if ((labirint[heightRandom - 1, widthRandom] == 'C' && labirint[heightRandom + 1, widthRandom] == 'C' && labirint[heightRandom, widthRandom - 1] != 'C' && labirint[heightRandom, widthRandom + 1] != 'C') ||
                        (labirint[heightRandom - 1, widthRandom] != 'C' && labirint[heightRandom + 1, widthRandom] != 'C' && labirint[heightRandom, widthRandom - 1] == 'C' && labirint[heightRandom, widthRandom + 1] == 'C'))
                    {
                        labirint[heightRandom, widthRandom] = '*';
                        countOfPassage--;
                    }
                }
            }

        }

        //Вспомогательный метод, который сканирует соседние комнаты и возвращает словарь, где значением являются еще непосещенные соседние комнаты, а ключем их расположение относительно текущей
        private Dictionary<char, Room> GetNeibours(Room room)
        {
            Dictionary<char, Room> dict = new Dictionary<char, Room>();

            //Комната слева
            if (room.width != 1 && labirint[room.height, room.width - 2] == 'K')
                dict.Add('L', new Room(room.height, room.width - 2));
            //Комната сверху
            if (room.height != 1 && labirint[room.height - 2, room.width] == 'K')
                dict.Add('U', new Room(room.height - 2, room.width));
            //Комната справа
            if (room.width < hidenWidth - 2 && labirint[room.height, room.width + 2] == 'K')
                dict.Add('R', new Room(room.height, room.width + 2));
            //Комната снизу
            if (room.height < hidenHeight - 2 && labirint[room.height + 2, room.width] == 'K')
                dict.Add('D', new Room(room.height + 2, room.width));

            return dict;
        }
       

        //Метод возвращающий сгенерированный лабиринт
        public char[,] ReleaseLab()
        {
            return labirint;
        }

        public override string ToString()
        {
            return string.Format("{0} to {1}", width, height);
        }
    }    

    public GameObject wall = null;
    public GameObject ground = null;   
    public GameObject player = null;
    public GameObject zombie = null;
    public static char[,] labirint;    
    bool isOneZombie; //один ли зомби на уровне
    public static float timeOfGame; 
    

    Labirint lab;

    void Start()
    {
        lab = new Labirint(10, 6);
        labirint = lab.ReleaseLab();
        Time.timeScale = 1;
        timeOfGame = 0;

        //расположение объектов на уровне 
        for (int i = 0; i < labirint.GetLength(0); i++)
        { 
            for (int j = 0; j < labirint.GetLength(1); j++)
            {
                if (labirint[i, j] == 'C')
                    Instantiate(wall, new Vector2(i, j), Quaternion.identity);
                else if (labirint[i, j] == 'S')
                {
                    Instantiate(wall, new Vector2(i, j), Quaternion.identity);
                    Instantiate(player, new Vector2(i , j + 1), Quaternion.identity);
                    labirint[i, j] = 'C';
                }
                else if (labirint[i, j] == 'F')
                {
                    Instantiate(wall, new Vector2(i, j), Quaternion.identity);
                    Instantiate(zombie, new Vector2(i, j - 1), Quaternion.identity);
                    isOneZombie = true;
                    labirint[i, j] = 'C';
                }
                else
                    Instantiate(ground, new Vector2(i, j), Quaternion.identity);
            }
        }        
    }
        
	void FixedUpdate () {
        
        //выполнены ли условия для добавления второго противника
        if(ScoreScipt.scoreValue >= 3 && isOneZombie)
        {
            AddZombie();
            isOneZombie = false;
        }
        timeOfGame += Time.deltaTime;        
    }    

    //добавляет противника в дальнем от игрока углу лабиринта
    void AddZombie()
    {
        int x;
        int y;

        if (PlayerController.playerPosition.x > labirint.GetLength(0) / 2) x = 1; else x = labirint.GetLength(0) - 2;
        if (PlayerController.playerPosition.y > labirint.GetLength(1) / 2) y = 1; else y = labirint.GetLength(1) - 2;

        Instantiate(zombie, new Vector2(x, y), Quaternion.identity);
    }

    
}
