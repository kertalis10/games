﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    List<Vector2> allObjectsInLabirint; //На будущее, для отслеживания всех подбираемых объктов в лабиринте, с целье избегания наложения   
    List<Vector2> coins ;
    public GameObject coin;
    
    //добавление подбираемого объекта в лабиринт
    void AddObjectToLabirint(ref List<Vector2> list)
    {
        int x;
        int y;
        bool isAded = false;

        do
        {
            x = Random.Range(1, MainScript.labirint.GetLength(0) - 1);
            y = Random.Range(1, MainScript.labirint.GetLength(1) - 1);
            if (!(list.Contains(new Vector2(x, y)) || MainScript.labirint[x, y] == 'C'))
            {           
                Instantiate(coin, new Vector2(x, y), Quaternion.identity);
                list.Add(new Vector2(x, y));
                isAded = true;
            }
        } while (!isAded);
    }


    Rigidbody2D rb;

    float playerSpeed = 2f;
    int x;
    int y;
    float timer;   
    public static Vector2 playerPosition;
    public static bool deadFromZombie = false;
    Vector3 theScale;
    

    void Start()
    {        
        coins = new List<Vector2>();
        rb = GetComponent<Rigidbody2D>();
        timer = 5f;   
    }

    void Update()
    {   
        playerPosition = transform.position;
        theScale = transform.localScale;

        if (coins.Count < 10 && timer >= 5)
        {
            AddObjectToLabirint(ref coins);
            timer = 0;
        }

        timer += Time.deltaTime;

        PlayerMove();
    }

    //Перемещение игрока, отслеживается положение игрока относительно центра проходов
    void PlayerMove()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        rb.velocity = Vector2.zero;

        x = (int)transform.position.x;
        y = (int)System.Math.Round(transform.position.y, 0);


        if (moveHorizontal > 0 && x < MainScript.labirint.GetLength(0) && MainScript.labirint[x + 1, y] != 'C' && y - transform.position.y < 0.2f && y - transform.position.y > -0.2f)
        {
            rb.velocity = new Vector2(moveHorizontal * playerSpeed, 0);
            Flip(theScale, 1);
        }
        if (moveHorizontal < 0 && x > 0 && MainScript.labirint[x, y] != 'C' && y - transform.position.y < 0.2f && y - transform.position.y > -0.2f)
        {
            rb.velocity = new Vector2(moveHorizontal * playerSpeed, 0);
            Flip(theScale, -1);
        }

        x = (int)System.Math.Round(transform.position.x, 0);
        y = (int)transform.position.y;

        if (moveVertical > 0 && y < MainScript.labirint.GetLength(1) && MainScript.labirint[x, y + 1] != 'C' && x - transform.position.x < 0.2f && x - transform.position.x > -0.2f)
        {
            rb.velocity = new Vector2(0, moveVertical * playerSpeed);
        }
        if (moveVertical < 0 && y > 0 && MainScript.labirint[x, y] != 'C' && x - transform.position.x < 0.2f && x - transform.position.x > -0.2f)
        {
            rb.velocity = new Vector2(0, moveVertical * playerSpeed);
        }
    }

    //Подбор монет и столкновение с противником
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            coins.Remove(new Vector2((int)other.gameObject.transform.position.x, (int)other.gameObject.transform.position.y));
            other.gameObject.SetActive(false);            
            ScoreScipt.scoreValue++;
        }
        if (other.gameObject.CompareTag("Zombie"))
        {
            deadFromZombie = true;
            Application.Quit();           
        }
    }

    //Поворот объекта
    void Flip(Vector3 theScale, int i)
    {
        theScale.x = i;
        transform.localScale = theScale;
    }

    

}
