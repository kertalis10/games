﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour {    

    public InputField nameField;
    public static string nameOfPlayer;
    public Transform canvasMenu;
    public Transform canvasResult;
    public Text text;

    private void Start()
    {
        
    }

    void FixedUpdate()
    {
        //Скрытие таблицы с результатами
        if (Input.GetKeyDown(KeyCode.Escape) && canvasResult.gameObject.activeInHierarchy == true)
        {
            
        }

    }

    public void Back()
    {
        canvasMenu.gameObject.SetActive(true);
        canvasResult.gameObject.SetActive(false);
    }

    //Вывод на экран таблицы с результатами
    public void LoadCanvasResult()
    {
        canvasMenu.gameObject.SetActive(false);
        canvasResult.gameObject.SetActive(true);
        text.text = OutputResults(LoadResults<LoadSaveResultsScript.PlayerInfo>("gameinfo.xml"), 20);
    }

    //Загрузка сцены с лабиринтом, считывание имени игрока
    public void LoadScene()
    {
        
        SceneManager.LoadScene("1");

        if (nameField.text.Trim() != "")
            nameOfPlayer = nameField.text;
        else
            nameOfPlayer = "Noname";
    }

   
    //Выход из приложения
    public void Quit()
    {
        Application.Quit();
    }

    //Загрузка результатов из файла
    List<T> LoadResults<T>(string path)
    {
        List<T> list = new List<T>();

        System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

        if (File.Exists(path))
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                list = (List<T>)formatter.Deserialize(fs);
            }
        }

        return list;
    }

    //Создание строки с результатами
    string OutputResults<T>(List<T> list, int countOfRows)
    {
        string s = "";

        if (countOfRows > list.Count) countOfRows = list.Count;

        for (int i = 0; i < countOfRows; i++)
            s += list[i].ToString() + "\n";

        return s;
    }
}
