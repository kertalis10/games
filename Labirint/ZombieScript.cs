﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieScript : MonoBehaviour {   

    List<char> nearbyCells;   
    char nextCell;
    char prevCell;
    Vector2 target;
    float zombieSpeed = 1f;
    float startZombieSpeed;
    Vector3 theScale;




    void Start () {
        target = transform.position;
        startZombieSpeed = zombieSpeed;
    }

    void FixedUpdate() {
        theScale = transform.localScale;

        //увеличение скорости
        if (ScoreScipt.scoreValue > 10)
        {
            zombieSpeed = startZombieSpeed + (ScoreScipt.scoreValue - 10) * 0.05f;            
        }

        //если персонаж пришел к целевым координатам, происходит выбор алгоритма следующего перемещения...
        if (System.Math.Round(target.x, 0) == System.Math.Round(transform.position.x, 0) && System.Math.Round(target.y, 0) == System.Math.Round(transform.position.y, 0) &&
            transform.position.x - (int)transform.position.x < 0.1f && transform.position.y - (int)transform.position.y < 0.1f)
        {
            if (ScoreScipt.scoreValue < 5)
                ZombieRandomWay();
            else
                ZombieShortestWay();
        }

        //...и направление перемещения
        switch (nextCell)
        {
            case 'R':
                transform.Translate(Vector2.right * Time.deltaTime * zombieSpeed);
                Flip(theScale, -1);
                break;
            case 'L':
                transform.Translate(Vector2.left * Time.deltaTime * zombieSpeed);
                Flip(theScale, 1);
                break;
            case 'U':
                transform.Translate(Vector2.up * Time.deltaTime * zombieSpeed);
                break;
            case 'D':
                transform.Translate(Vector2.down * Time.deltaTime * zombieSpeed);
                break;
        }


    }    

    //Случайное перемещение
    void ZombieRandomWay()
    {
        
            
            nearbyCells = CheckNearbyCells((int)transform.position.x, (int)transform.position.y);

            //если направление со стороны которого персонаж пришел, не единственное возможное к дальнейшему перемещению, это направление исключается 
            if (nearbyCells.Count > 1)
                nearbyCells.Remove(prevCell);

            nextCell = nearbyCells[Random.Range(0, nearbyCells.Count)];


            switch (nextCell)
            {
                case 'R':
                    target = new Vector2(target.x + 1, target.y);
                    prevCell = 'L';
                    break;
                case 'L':
                    target = new Vector2(target.x - 1, target.y);
                    prevCell = 'R';
                    break;
                case 'U':
                    target = new Vector2(target.x, target.y + 1);
                    prevCell = 'D';
                    break;
                case 'D':
                    target = new Vector2(target.x, target.y - 1);
                    prevCell = 'U';
                    break;
            }     

            
    }    

    //Поиск кратчайщего пути к игроку
    //Алгоритм работает следующим образом: в массиве хранятся массивы с координатами всех возможных путей расходящихся от координат персонажа(зомби).
    //Если путь приходит в тупик, или заходит на уже посещенную клетку, этот путь прекращается
    //Как только один из путей приходит в точку с координатами, аналогичными координатам игрока, запускается перемещение по этому пути
    void ZombieShortestWay()
    {  
        List<List<Vector2>> tempListOfWays = new List<List<Vector2>>();
        tempListOfWays.Add(new List<Vector2>());        
        tempListOfWays[0].Add(new Vector2((int)transform.position.x, (int)transform.position.y));
        bool isWayExist;
        bool isWayFound = false;
               
            do
            {
                List<List<Vector2>> listOfWays = new List<List<Vector2>>(tempListOfWays);
                for (int i = 0; i < listOfWays.Count; i++)
                {
                    Vector2 currentRoom = listOfWays[i][listOfWays[i].Count - 1];

                    nearbyCells = CheckNearbyCells((int)currentRoom.x, (int)currentRoom.y);

                    isWayExist = false;

                    for (int j = 0; j < nearbyCells.Count; j++)
                        if (!listOfWays[i].Contains(CheckRoom(currentRoom, nearbyCells[j])))
                        {
                            if (!isWayExist)
                            {
                                tempListOfWays[i].Add(CheckRoom(currentRoom, nearbyCells[j]));
                                isWayExist = true;
                            }
                            else
                            {
                                List<Vector2> tempList = new List<Vector2>(listOfWays[i]);
                                tempList.RemoveAt(tempList.Count - 1);
                                tempList.Add(CheckRoom(currentRoom, nearbyCells[j]));
                                tempListOfWays.Add(tempList);
                            }
                        }
                }
                foreach (var v in tempListOfWays)
                {
                    if (v[v.Count - 1].x == (int)System.Math.Round(PlayerController.playerPosition.x, 0) && v[v.Count - 1].y == (int)System.Math.Round(PlayerController.playerPosition.y, 0))
                    {
                        isWayFound = true;
                        //Определение стороны следующей в пути клетки относительно текущей
                        if (v[1].x - transform.position.x > 0.5) nextCell = 'R';
                        if (v[1].x - transform.position.x < -0.5) nextCell = 'L';
                        if (v[1].y - transform.position.y > 0.5) nextCell = 'U';
                        if (v[1].y - transform.position.y < -0.5) nextCell = 'D';
                        target.x = v[1].x;
                        target.y = v[1].y;
                    }
                }
            } while (!isWayFound);             
    }    

    //Возвращение координат комнаты, в зависимости расположение от текущей
    Vector2 CheckRoom(Vector2 room, char c)
    {
        if (c == 'R') return new Vector2(room.x + 1, room.y);
        else if (c == 'L') return new Vector2(room.x - 1, room.y);
        else if (c == 'U') return new Vector2(room.x, room.y + 1);
        else if (c == 'D') return new Vector2(room.x, room.y - 1);
        else return new Vector2(room.x, room.y);
    }
    //Возвращение массива сторон с комнатами, в зависимости от заданных координат 
    List<char> CheckNearbyCells(int x, int y)
    {     
        List<char> list = new List<char>();

        if (MainScript.labirint[x + 1, y] != 'C')
            list.Add('R');
        if (MainScript.labirint[x - 1, y] != 'C')
            list.Add('L');
        if (MainScript.labirint[x, y + 1] != 'C')
            list.Add('U');
        if (MainScript.labirint[x, y - 1] != 'C')
            list.Add('D');

        return list;
    }

    //Поворот объекта
    void Flip(Vector3 theScale, int i)
    {
        theScale.x = i;
        transform.localScale = theScale;
    }
}
