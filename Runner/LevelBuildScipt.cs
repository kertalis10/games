﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuildScipt : MonoBehaviour {

    int tileCountInPool = 10;
    int tileCountOnScene = 5;
    float levelBuildClobalPosition;

    TileFactoryScipt.TileFactory factory;
    ObjectPoolsScript.TileObjectPool tileObjectPool;
    ObjectPoolsScript.CoinObjectPool coinObjectPool;
    ObjectPoolsScript.ShellObjectPool shellObjectPool;
    Queue<TileFactoryScipt.Tile> queue;

    // Use this for initialization
    void Start () {
        levelBuildClobalPosition = 0;
        factory = new TileFactoryScipt.TileFactory();
        tileObjectPool = new ObjectPoolsScript.TileObjectPool();
        coinObjectPool = new ObjectPoolsScript.CoinObjectPool();
        shellObjectPool = new ObjectPoolsScript.ShellObjectPool();
        queue = new Queue<TileFactoryScipt.Tile>();

        //В пуле не может быть тайлов меньше чем на сцене
        tileCountInPool = tileCountInPool > tileCountOnScene ? tileCountInPool : tileCountOnScene + 1;

        for (int i = 0; i < tileCountInPool; i++)
            tileObjectPool.Add(factory.ReturnTile(TileFactoryScipt.TileTypes.industrial));

        queue.Enqueue(tileObjectPool.GetTile(levelBuildClobalPosition));
        levelBuildClobalPosition += 200;

        for (int i = 1; i < tileCountOnScene; i++)
        {
            AddTile();            
            levelBuildClobalPosition += 200;
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (PlayerController.playerPosition > levelBuildClobalPosition - (tileCountOnScene - 1) * 200 && PlayerController.timeOfSession > 1)
        {
            AddTile();
            queue.Peek().SetActive(false);
            queue.Dequeue();
            levelBuildClobalPosition += 200;
        }
	}

    void AddTile()
    {
        TileFactoryScipt.Tile tile = tileObjectPool.GetTile(levelBuildClobalPosition);
        if (tile as TileFactoryScipt.TileIndustrialType != null)
        {
            ((TileFactoryScipt.TileIndustrialType)tile).AddCoinsAndShellsToTile(coinObjectPool, shellObjectPool);
        }
        queue.Enqueue(tile);       
    }
}
