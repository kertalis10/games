﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LoadSaveResultScript : MonoBehaviour {

    [System.Serializable]
    public struct PlayerInfo
    {
        public string name;
        public int score;
        public string time;

        public PlayerInfo(string _name, int _score, string _time)
        {
            name = _name;
            score = _score;
            time = _time;
        }

        public override string ToString()
        {
            return string.Format("{0}\t\t{1}\t\t{2}", name, score, time);
        }
    }

    //Загрузка результатов из файла
    public static List<T> LoadResults<T>(string path)
    {
        List<T> list;

        System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
                list = (List<T>)formatter.Deserialize(fs);
        }
        catch
        {
            list = new List<T>();
        }


        return list;
    }

    //Сохранение результатов в файл
    public static void SaveResults<T>(List<T> list, string path)
    {
        System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

        try
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                formatter.Serialize(fs, list);
        }
        catch
        {
        }
    }
}
