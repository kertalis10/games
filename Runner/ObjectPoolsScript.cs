﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolsScript : MonoBehaviour {

    public class TileObjectPool
    {

        List<TileFactoryScipt.Tile> list;
        int i = 0;

        public TileObjectPool()
        {
            list = new List<TileFactoryScipt.Tile>();           
        }

        public void Add(TileFactoryScipt.Tile tile)
        {
            list.Add(tile);
        }

        public TileFactoryScipt.Tile GetTile(float f)
        {
            i = i < list.Count ? i : 0;

            list[i].ChangePosition(f);
            list[i].SetActive(true);  
            return list[i++];            
        }
    }
    
    public class CoinObjectPool
    {
        List<GameObject> list;
        string path = "ILS/ILS 1";

        public CoinObjectPool()
        {
            list = new List<GameObject>();
        }
        
        public GameObject GetObject()
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].activeSelf == false)
                {
                    list[i].SetActive(true);
                    list[i].transform.GetChild(0).gameObject.SetActive(true);
                    return list[i];
                }
            }

            GameObject newGameObject;
            newGameObject = Instantiate(Resources.Load<GameObject>(path));
            list.Add(newGameObject);
            return newGameObject;
        }

        public override string ToString()
        {
            return list.Count.ToString();
        }
    }

    public class ShellObjectPool
    {
        List<GameObject> list;
        string[] path = { "Bomb/B1pref", "Bomb/B2pref" };

        public ShellObjectPool()
        {
            list = new List<GameObject>();
        }

        public GameObject GetObject()
        {

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].activeSelf == false)
                {
                    list[i].SetActive(true);                    
                    return list[i];
                }
            }  

            GameObject newGameObject;
            newGameObject = Instantiate(Resources.Load<GameObject>(path[Random.Range(0, path.Length)]));
            list.Add(newGameObject);

            return newGameObject;
        }

        public override string ToString()
        {
            return list.Count.ToString();
        }
    }

}
