﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{

    public Transform canvas;
    
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

        if (PlayerController.life <= 0)
        {
            EndGame();
        }

    }

    void Pause()
    {
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            canvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    void EndGame()
    {
        canvas.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void Quit()
    {
        SaveResult();
        Application.Quit();        
    }

    public void QuittoMenu()
    {
        SaveResult();
        SceneManager.LoadScene("0");
    }

    public void LoadScene()
    {
        SaveResult();
        SceneManager.LoadScene("1");
    }

    void SaveResult()
    {
        List<LoadSaveResultScript.PlayerInfo> list = LoadSaveResultScript.LoadResults<LoadSaveResultScript.PlayerInfo>("gameinfo.xml");

        LoadSaveResultScript.PlayerInfo pl = new LoadSaveResultScript.PlayerInfo(StartMenuScript.nameOfPlayer, PlayerController.score, "");

        list.Add(pl);

        LoadSaveResultScript.SaveResults(list, "gameinfo.xml");

    }
}
    
