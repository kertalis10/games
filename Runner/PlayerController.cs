﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    int line = 2;       //Линия по которой дижется игрок. Варианты 1-3. Старт с центральной. 
    Rigidbody rb;
    public bool isGround;
    bool isLongJump;
    float position;
    float centerposition;
    bool isOnLine = true;
    bool isMovingLeft = false;
    bool isMovingRight = false;
    public bool isAddedForce = false;
    bool isEndedTimeWithOutCollider;
    int playerSpeed;    
    float timeWithOutCollider;
    public static float playerPosition;
    public static int score;
    public static int life;
    public static float timeOfSession;

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        rb = GetComponent<Rigidbody>();
        transform.position = new Vector3( -15, 23, -70 );
        position = centerposition = transform.position.x;
        isEndedTimeWithOutCollider = true;
        timeWithOutCollider = 0;
        score = 0;
        timeOfSession = 0;
        life = 3;
    }

    // Update is called once per frame
    void Update()
    {
        IsColliderDelete();

        timeOfSession += Time.deltaTime;

        rb.AddForce(transform.up * -15);

        transform.position += transform.forward * Time.deltaTime * 25;

        playerPosition = transform.position.z;

        if (transform.position.y < 20)
            Death();

        if (Input.GetKeyDown(KeyCode.A))
        {
            MoveLeft();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            MoveRight();
        }

        if (isMovingLeft)
        {
            transform.position += transform.right * Time.deltaTime * -15;

            if (transform.position.x < position)
            {
                isOnLine = true;
                isMovingLeft = false;
            }
        }

        if (isMovingRight)
        {
            transform.position += transform.right * Time.deltaTime * 15;

            if (transform.position.x > position)
            {
                isOnLine = true;
                isMovingRight = false;
            }
        }

        Jump();

    }



    void Jump()
    {
        Ray ray = new Ray(gameObject.transform.position, Vector3.down);
        RaycastHit rh;

        if (Physics.Raycast(ray, out rh, 1.5f))
        {
            isGround = true;
        }
        else
        {
            isGround = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGround && isOnLine)
            rb.AddForce(transform.up * 450);

        if (Input.GetKeyDown(KeyCode.LeftShift) && isGround && isOnLine)
            rb.AddForce(transform.up * 900);
    }

    void MoveLeft()
    {
        if (line > 1 && isOnLine)
        {
            line--;
            isOnLine = false;
            isMovingLeft = true;
            if (line == 1) position = centerposition - 4;
            if (line == 2) position = centerposition + 0.1f;
        }
    }

    void MoveRight()
    {
        if (line < 3 && isOnLine)
        {
            line++;
            isOnLine = false;
            isMovingRight = true;
            if (line == 2) position = centerposition - 0.1f;
            if (line == 3) position = centerposition + 4;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Coin"))
        {
            other.gameObject.SetActive(false);
            score++;
        }

        if (other.gameObject.tag.Contains("Block") && isEndedTimeWithOutCollider)
        {
            Death();
        }
    }

    void Death()
    {
        if (life != 0)
        {
            transform.position = new Vector3(transform.position.x, 23, transform.position.z - transform.position.z % 200 + 25);
            life--;
            isEndedTimeWithOutCollider = false;
            timeWithOutCollider = 3;
        }
        else
        {
            //Time.timeScale = 0;            
        }       
    }

    void IsColliderDelete()
    {
        if (timeWithOutCollider > 0)
            timeWithOutCollider -= Time.deltaTime;
        else
            isEndedTimeWithOutCollider = true;
    }
}

class Player
{
    private static Player instance;

    private Player()
    { }

    public static Player getInstance()
    {
        if (instance == null)
            instance = new Player();
        return instance;
    }
}
