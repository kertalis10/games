﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if(gameObject.tag.Contains("Score"))
            text.text = "Score: " + PlayerController.score;
        else if(gameObject.tag.Contains("Life"))
            text.text = "Life: " + PlayerController.life;


    }
}
