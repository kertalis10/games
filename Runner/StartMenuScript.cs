﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenuScript : MonoBehaviour {
    public InputField nameField;
    public static string nameOfPlayer;
    public Transform canvasMenu;
    public Transform canvasResult;
    public Text name;
    public Text score;


    private void Start()
    {

    }

    void FixedUpdate()
    {
        //Скрытие таблицы с результатами
        if (Input.GetKeyDown(KeyCode.Escape) && canvasResult.gameObject.activeInHierarchy == true)
        {
            canvasMenu.gameObject.SetActive(false);
            canvasResult.gameObject.SetActive(true);
        }

    }
  
    //Вывод на экран таблицы с результатами
    public void LoadCanvasResult()
    {
        canvasMenu.gameObject.SetActive(false);
        canvasResult.gameObject.SetActive(true);
        
        List<LoadSaveResultScript.PlayerInfo> list = LoadSaveResultScript.LoadResults<LoadSaveResultScript.PlayerInfo>("gameinfo.xml").OrderByDescending(x => x.score).ToList();
        
        int count;

        if (list.Count > 10)
            count = 10;
        else
            count = list.Count;

        for (int i = 0; i < count; i++)
        {
            name.text += list[i].name + "\n";
            score.text += list[i].score + "\n";
        }        
    }

    public void ReturnToMainMenu()
    {
        canvasMenu.gameObject.SetActive(true);
        canvasResult.gameObject.SetActive(false);
    }

    //Загрузка сцены с лабиринтом, считывание имени игрока
    public void LoadScene()
    {      

        SceneManager.LoadScene("1");
       
        if (nameField.text.Trim() != "")
            nameOfPlayer = nameField.text;
        else
            nameOfPlayer = "Player";
    }


    //Выход из приложения
    public void Quit()
    {
        Application.Quit();
    }    
}
