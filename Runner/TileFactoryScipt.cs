﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileFactoryScipt : MonoBehaviour
{ 
    public class TileFactory
    {
        public TileIndustrialType ReturnTile(TileTypes tileType)
        {
            switch (tileType)
            {
                case TileTypes.industrial:
                    return new TileIndustrialType();
                default:
                    return new TileIndustrialType();
            }
        }
    }

    public enum TileTypes
    {
        industrial = 1
    } 
    
    public abstract class Tile
    {
        protected GameObject go;
        protected int currentFloorShift = 0;

        public bool IsActive()
        {
            return go.activeSelf;
        }

        public void SetActive(bool b)
        {
            go.SetActive(b);
        }

        public void ChangePosition(float f)
        {
            go.transform.position = new Vector3(0, 0, f);
        }

        public float Position()
        {
            return go.transform.position.z;
        }

        protected void InstantiateObject(GameObject prefab, GameObject parent, Vector3 vector, float rotationY, float rotationZ)
        {
            GameObject newGameObjectInstantiated;

            newGameObjectInstantiated = Instantiate(prefab);
            newGameObjectInstantiated.transform.position = vector;
            newGameObjectInstantiated.transform.rotation = Quaternion.Euler(0, rotationY, rotationZ);
            newGameObjectInstantiated.transform.parent = parent.transform;
        }    
    }

    public class TileIndustrialType : Tile
    {
        public TileIndustrialType()
        {
            //Создаем пустой контейнер для тайла
            go = new GameObject();
            go.SetActive(false);
            //Пол
            InstantiateObject(Resources.Load<GameObject>("Floor/Floor"), go, new Vector3(), 0, 0);
            //Стены
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(40, 0, -50), 0, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(40, 0, 0), 0, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(40, 0, 50), 0, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(40, 0, 100), 0, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(-40, 0, -50), 180, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(-40, 0, 0), 180, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(-40, 0, 50), 180, 0);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(-40, 0, 100), 180, 0);

            //Потолок
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(62.5f, 60, -50), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(62.5f, 60, 0), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(62.5f, 60, 50), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(62.5f, 60, 100), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(0, 60, -50), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(0, 60, 0), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(0, 60, 50), 0, 90);
            InstantiateObject(Resources.Load<GameObject>("Wall/Wall"), go, new Vector3(0, 60, 100), 0, 90);
            //Лампы
            for (int i = -50; i <= 50; i += 100)
            {
                InstantiateObject(Resources.Load<GameObject>("Lamp/Lamp_Fix 3 1"), go, new Vector3(-15, 65, i), 0, 0); 
                InstantiateObject(Resources.Load<GameObject>("Lamp/Lamp_Fix 3 1"), go, new Vector3(15, 65, i), 0, 0);

                if(Random.Range(0, 4) == 1)
                    InstantiateObject(Resources.Load<GameObject>("Matza/Matza Light"), go, new Vector3(-15, 45, i), 0, 0);
            }
            //Трубы и кабели
            InstantiateObject(Resources.Load<GameObject>("PropsFix/FIX_Tube_Cable 1"), go, new Vector3(-40, 35, -100), 180, 0);

            AddConvToTile(-23);
        }       

        void AddConvToTile(float Xpos)
        {
            ReturnPartOfConv(1, 2, 2, 2, 2, 0, Xpos);

            if (currentFloorShift == 10)
            {
                ReturnPartOfConv(0, 1, 1, 1, 0, 0, Xpos);

                if (currentFloorShift == 90)
                {
                    ReturnPartOfConv(0, 0, 0, 0, 1, 1, Xpos);
                    ReturnPartOfConv(0, 0, 1, 0, 1, 0, Xpos);
                }               

                if (currentFloorShift == 110)
                {
                    ReturnPartOfConv(0, 0, 0, 0, 1, 1, Xpos);
                    ReturnPartOfConv(0, 0, 0, 1, 0, 0, Xpos);
                }               
            }

            if (currentFloorShift == 80)
            {
                ReturnPartOfConv(0, 0, 0, 0, 1, 1, Xpos);
                ReturnPartOfConv(0, 0, 1, 0, 1, 0, Xpos);

                if (currentFloorShift == 100)
                {                    
                    ReturnPartOfConv(0, 0, 0, 1, 0, 0, Xpos);
                }
            }

            if (currentFloorShift == 100)
            {
                ReturnPartOfConv(0, 0, 0, 0, 1, 1, Xpos);
                ReturnPartOfConv(0, 0, 0, 1, 1, 0, Xpos);

                if (currentFloorShift == 120)
                {
                    ReturnPartOfConv(0, 0, 0, 1, 0, 0, Xpos);
                }
            }

            if (currentFloorShift == 180)
            {
                ReturnPartOfConv(0, 0, 0, 0, 1, 0, Xpos);
                ReturnPartOfConv(0, 0, 0, 0, 0, 1, Xpos);
            }

            if (currentFloorShift == 190)
            {
                ReturnPartOfConv(0, 0, 0, 0, 0, 1, Xpos);
            }


        }

        GameObject ReturnPartOfConv(int twentyConv, int eighteenConv, int tenConv, int eightConv, int natasha, int empty, float xPos)
        {
            int sum = twentyConv + eighteenConv + tenConv + eighteenConv + natasha + empty;

            int r = Random.Range(1, sum + 1);

            if (r <= twentyConv) return CreatePartOfConv(20, xPos);
            else if (r <= twentyConv + eighteenConv) return CreatePartOfConv(18, xPos);
            else if (r <= twentyConv + eighteenConv + tenConv) return CreatePartOfConv(10, xPos);
            else if (r <= twentyConv + eighteenConv + tenConv + eightConv) return CreatePartOfConv(8, xPos);
            else if (r <= twentyConv + eighteenConv + tenConv + eightConv + natasha) return CreateNatasha(xPos);
            else
            {
                currentFloorShift += 10;
                return null;                
            }
        }

        GameObject CreatePartOfConv(int length, float xPos)
        {
            GameObject convGO = new GameObject();

            string[] path = { "Laser/LaserAnim FA Fix 1 1 1" , "LaserUp/L2 A 1 3" };

            InstantiateObject(Resources.Load<GameObject>("FixConv/Part3"), convGO, new Vector3(xPos, 22.3f, currentFloorShift - 100 + 20), 0, 0);

            for (int i = 0; i < (length / 2 - 2); i++)
                InstantiateObject(Resources.Load<GameObject>("FixConv/Part2"), convGO, new Vector3(xPos, 22.3f, currentFloorShift - 100 + (i * 20) + 20), 0, 0);

            InstantiateObject(Resources.Load<GameObject>(path[Random.Range(0, path.Length)]), convGO, new Vector3(xPos + 7, 21.3f, currentFloorShift - 30), 0, 0);           

            InstantiateObject(Resources.Load<GameObject>("FixConv/Part1"), convGO, new Vector3(xPos, 22.3f, currentFloorShift - 100 + (length * 10) - 20), 0, 0);

            convGO.name = length.ToString();

            convGO.transform.parent = go.transform;

            currentFloorShift += length * 10;

            return convGO;
        }

        GameObject CreateNatasha(float xPos)
        {
            GameObject natasha = new GameObject();

            InstantiateObject(Resources.Load<GameObject>("Natasha/Natasha 1 1"), natasha, new Vector3(xPos, 21f, currentFloorShift - 100 + 10), 0, 0);

            currentFloorShift += 10;

            natasha.transform.parent = go.transform;

            return natasha;
        }

        public void AddCoinsAndShellsToTile(ObjectPoolsScript.CoinObjectPool coinsPool, ObjectPoolsScript.ShellObjectPool shellsPool)
        {
            List<float> list = new List<float>();

            var listofTransorms = go.GetComponentsInChildren<Transform>().ToList<Transform>().Where(s => s.tag.Contains("Inside")).ToList<Transform>();

            list.Add(listofTransorms[0].transform.position.z - 10);
            list.Add(listofTransorms[0].transform.position.z - 5);

            foreach (var v in listofTransorms)
                for (int i = 0; i <= 15; i += 5)
                    list.Add(v.transform.position.z + i);

            list.Add(list[list.Count - 1] + 5);
            list.Add(list[list.Count - 1] + 5);

            int currentLength = 0;
            int numberOfLine;

            while (currentLength < list.Count)
            {
                int lenghtOfCoinRoad = Random.Range(0, 5);
                if (currentLength + lenghtOfCoinRoad > list.Count) lenghtOfCoinRoad = list.Count - currentLength;

                numberOfLine = Random.Range(0, 3);
                for (int i = 0; i < lenghtOfCoinRoad; i++)                   
                    coinsPool.GetObject().transform.position = new Vector3(-23 + 3.85f + numberOfLine * 4.15f, 23.7f, list[currentLength + i]);
                currentLength += lenghtOfCoinRoad;
            }

            int controlOfPass = 0;  // Недопущение постановки снарядов без возможности прохода между ними

            for (int i = 0; i < list.Count; i++)
            {
                numberOfLine = Random.Range(0, 3);


                if (Random.Range(0, 2) == 1)
                {
                    if (controlOfPass < 2)
                    {
                        controlOfPass++;
                        shellsPool.GetObject().transform.position = new Vector3(-23 + 3.85f + numberOfLine * 4.15f, 22f, list[i]);
                    }
                    else
                    {
                        controlOfPass = 0;
                    }
                }
            }
        }        
    }
}
   